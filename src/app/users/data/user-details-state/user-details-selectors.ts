import { createSelector } from '@ngrx/store';
import { userDetailsFeature } from './user-details-reducers';

const { selectUser, selectLoading, selectMeetings } = userDetailsFeature;

const selectUserMeetings = createSelector(
  selectMeetings,
  (meetings) => meetings?.items
);

export const userDetailsQuery = {
  selectUser,
  selectLoading,
  selectUserMeetings,
};
