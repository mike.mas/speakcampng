import { createFeature, createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/core/models/meeting';
import { UserDetails } from 'src/app/core/models/user-details';
import { userDetailsActions } from './user-details-actions';

export interface UserDetailsState {
  user: UserDetails | null;
  loading: boolean;
  loaded: boolean;

  meetings: { items: Meeting[]; loading: boolean; loaded: boolean };
}

export const userDetailsInitialState: UserDetailsState = {
  user: null,
  loading: false,
  loaded: false,

  meetings: { items: [], loading: false, loaded: false },
};

export const userDetailsFeature = createFeature({
  name: 'userDetails',
  reducer: createReducer(
    userDetailsInitialState,

    on(userDetailsActions.loadUserDetails, (state) => {
      return { ...state, loading: true };
    }),
    on(userDetailsActions.loadUserDetailsSuccess, (state, { result }) => {
      return {
        ...state,
        user: result,
        loading: false,
        loaded: true,
      };
    }),
    on(userDetailsActions.loadUserDetailsFailure, (state) => {
      return {
        ...state,
        user: null,
        loading: false,
        loaded: false,
      };
    }),
    on(userDetailsActions.loadUserMeetings, (state) => {
      return {
        ...state,
        meetings: { items: [], loading: true, loaded: false },
      };
    }),
    on(userDetailsActions.loadUserMeetingsSuccess, (state, { result }) => {
      return {
        ...state,
        meetings: { items: result.items, loading: false, loaded: true },
      };
    }),
    on(userDetailsActions.loadUserMeetingsFailure, (state) => {
      return {
        ...state,
        meetings: { items: [], loading: false, loaded: false },
      };
    })
  ),
});
