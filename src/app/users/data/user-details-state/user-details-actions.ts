import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/core/models/list-result';
import { Meeting } from 'src/app/core/models/meeting';
import { Review } from 'src/app/core/models/review';
import { UserDetails } from 'src/app/core/models/user-details';

export const userDetailsActions = createActionGroup({
  source: 'User Details',
  events: {
    'Load User Details': props<{ id: number }>(),
    'Load User Details Failure': props<{ error: Error }>(),
    'Load User Details Success': props<{ result: UserDetails }>(),

    'Load User Meetings': props<{ id: number }>(),
    'Load User Meetings Failure': props<{ error: Error }>(),
    'Load User Meetings Success': props<{ result: ListResult<Meeting> }>(),

    'Load User Reviews': props<{ id: number }>(),
    'Load User Reviews Failure': props<{ error: Error }>(),
    'Load User Reviews Success': props<{ result: ListResult<Review> }>(),
  },
});
