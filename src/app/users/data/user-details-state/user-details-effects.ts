import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { UsersService } from '../services/users.service';
import { userDetailsActions } from './user-details-actions';

@Injectable()
export class UserDetailsEffects {
  loadDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(userDetailsActions.loadUserDetails),
      switchMap((action) =>
        this.usersService.getUserDetails(action.id).pipe(
          map((result) =>
            userDetailsActions.loadUserDetailsSuccess({ result })
          ),
          catchError((error) =>
            of(userDetailsActions.loadUserDetailsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private usersService: UsersService,
    private store: Store
  ) {}
}
