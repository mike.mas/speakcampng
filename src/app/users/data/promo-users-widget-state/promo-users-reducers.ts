import { createFeature, createReducer, on } from '@ngrx/store';
import { User } from 'src/app/core/models/user';
import { promoUsersActions } from './promo-users-actions';

export interface PromoUsersState {
  users: User[];
  loading: boolean;
  loaded: boolean;
}

export const promoUsersInitialState: PromoUsersState = {
  users: [],
  loading: false,
  loaded: false,
};

export const promoUsersFeature = createFeature({
  name: 'promoUsers',
  reducer: createReducer(
    promoUsersInitialState,

    on(promoUsersActions.loadPromoUsers, (state) => {
      return { ...state, loading: true };
    }),
    on(promoUsersActions.loadPromoUsersSuccess, (state, { result }) => {
      return {
        ...state,
        users: result.items,
        loading: false,
        loaded: true,
      };
    }),
    on(promoUsersActions.loadPromoUsersFailure, (state) => {
      return {
        ...state,
        users: [],
        loading: false,
        loaded: false,
      };
    })
  ),
});
