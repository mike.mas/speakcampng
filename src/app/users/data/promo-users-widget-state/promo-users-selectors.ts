import { promoUsersFeature } from './promo-users-reducers';

const { selectUsers, selectLoading } = promoUsersFeature;

export const promoUsersQuery = {
  selectUsers,
  selectLoading,
};
