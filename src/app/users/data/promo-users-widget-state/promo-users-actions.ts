import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/core/models/list-result';
import { User } from 'src/app/core/models/user';

export const promoUsersActions = createActionGroup({
  source: 'Promo Users',
  events: {
    'Load Promo Users': props<{ top: number }>(),
    'Load Promo Users Failure': props<{ error: Error }>(),
    'Load Promo Users Success': props<{ result: ListResult<User> }>(),
  },
});
