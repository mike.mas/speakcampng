import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { UsersService } from '../services/users.service';
import { promoUsersActions } from './promo-users-actions';

@Injectable()
export class PromoUsersEffects {
  loadUserMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(promoUsersActions.loadPromoUsers),
      switchMap((action) =>
        this.usersService.getRatedUsers(action.top).pipe(
          map((result) => promoUsersActions.loadPromoUsersSuccess({ result })),
          catchError((error) =>
            of(promoUsersActions.loadPromoUsersFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private usersService: UsersService,
    private store: Store
  ) {}
}
