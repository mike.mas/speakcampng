import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ListResult } from 'src/app/core/models/list-result';
import { Meeting } from 'src/app/core/models/meeting';
import { Review } from 'src/app/core/models/review';
import { User } from 'src/app/core/models/user';
import { UserDetails } from 'src/app/core/models/user-details';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class UsersService {
  constructor(private http: HttpClient) {}

  getUserDetails(id: number): Observable<UserDetails> {
    return this.http.get(`${environment.mainApiUrl}Users/${id}`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getRatedUsers(top: number): Observable<ListResult<User>> {
    return this.http
      .get(
        `${environment.mainApiUrl}Users?userType=tutor&sorting=RatingDesc&pageSize=${top}`
      )
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }
}
