import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PromoUsersWidget } from './promo-users-widget.component';
import { PromoUsersEffects } from '../data/promo-users-widget-state/promo-users-effects';
import { promoUsersFeature } from '../data/promo-users-widget-state/promo-users-reducers';

@NgModule({
  declarations: [PromoUsersWidget],
  exports: [PromoUsersWidget],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(promoUsersFeature),
    EffectsModule.forFeature([PromoUsersEffects]),
  ],
})
export class PromoUsersWidgetModule {}
