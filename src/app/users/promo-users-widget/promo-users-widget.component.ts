import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/models/user';
import { promoUsersActions } from '../data/promo-users-widget-state/promo-users-actions';
import { promoUsersQuery } from '../data/promo-users-widget-state/promo-users-selectors';

@Component({
  selector: 'sc-promo-users-widget',
  templateUrl: './promo-users-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PromoUsersWidget implements OnInit {
  @Input()
  top: number = 4;

  users$: Observable<User[]>;

  constructor(private store: Store) {}

  ngOnChanges(): void {
    this.store.dispatch(promoUsersActions.loadPromoUsers({ top: this.top }));
  }

  ngOnInit(): void {
    this.users$ = this.store.select(promoUsersQuery.selectUsers);
  }
}
