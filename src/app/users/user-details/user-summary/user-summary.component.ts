import { Component, Input, OnInit } from '@angular/core';
import { UserDetails } from 'src/app/core/models/user-details';

@Component({
  selector: 'sc-user-summary',
  templateUrl: './user-summary.component.html',
})
export class UserSummaryComponent implements OnInit {
  @Input()
  user: UserDetails;

  constructor() {}

  ngOnInit(): void {}
}
