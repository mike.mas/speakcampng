import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { UserDetails } from 'src/app/core/models/user-details';
import { userDetailsActions } from '../data/user-details-state/user-details-actions';
import { userDetailsQuery } from '../data/user-details-state/user-details-selectors';

@Component({
  selector: 'sc-user-details',
  templateUrl: './user-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailsComponent implements OnInit {
  user$: Observable<UserDetails | null>;
  userId: number;

  constructor(private route: ActivatedRoute, private store: Store) {}

  ngOnInit(): void {
    this.userId = this.route.snapshot.params['id'];
    this.store.dispatch(
      userDetailsActions.loadUserDetails({ id: this.userId })
    );
    this.user$ = this.store.select(userDetailsQuery.selectUser);
  }
}
