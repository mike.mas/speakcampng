import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { UserDetailsComponent } from './user-details.component';
import { UserSummaryComponent } from './user-summary/user-summary.component';
import { UserReviewsWidgetModule } from 'src/app/reviews/user-reviews-widget/user-reviews-widget.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserDetailsEffects } from '../data/user-details-state/user-details-effects';
import { userDetailsFeature } from '../data/user-details-state/user-details-reducers';
import { UserMeetingsWidgetModule } from 'src/app/meetings/user-meetings-widget/user-meetings-widget.module';

@NgModule({
  declarations: [UserDetailsComponent, UserSummaryComponent],
  exports: [UserDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    UserReviewsWidgetModule,
    UserMeetingsWidgetModule,
    StoreModule.forFeature(userDetailsFeature),
    EffectsModule.forFeature([UserDetailsEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: UserDetailsComponent,
      },
    ]),
  ],
})
export class UserDetailsModule {}
