import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class DateHelper {
  getUTCDateString(date: Date | null): string {
    return !date ? '' : date.toUTCString();
  }
}
