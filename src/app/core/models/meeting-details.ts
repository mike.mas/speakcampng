import { Meeting } from './meeting';
import { Visit } from './visit';

export interface MeetingDetails extends Meeting {
  description: string;
  durationMinutes: number;
  placesLimit: number;
  placesOccupied: number;
  visits: Visit[];
}
