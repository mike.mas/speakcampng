import { User } from './user';

export interface UserDetails extends User {
  meetingsCount: number;
  creationDate: string;
  description: string;
}
