export interface ListResult<T> {
  count: number;
  page: number;
  pageSize: number;
  pagesCount: number;
  items: T[];
}
