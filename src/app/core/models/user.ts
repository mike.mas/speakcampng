import { Language } from './language';

export interface User {
  id: number;
  firstName: string;
  lastName: string;
  photoKey: string;
  rating: number;
  language: Language;
  languageLevel: string;
}
