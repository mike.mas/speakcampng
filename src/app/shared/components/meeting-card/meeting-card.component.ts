import { Component, Input, OnInit } from '@angular/core';
import { Meeting } from 'src/app/core/models/meeting';

@Component({
  selector: 'sc-meeting-card',
  templateUrl: './meeting-card.component.html',
})
export class MeetingCardComponent implements OnInit {
  constructor() {}

  @Input()
  meeting: Meeting;

  ngOnInit(): void {}
}
