import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user';

@Component({
  selector: 'sc-user-card',
  templateUrl: './user-card.component.html',
})
export class UserCardComponent implements OnInit {
  constructor() {}

  @Input()
  user: User;

  ngOnInit(): void {}
}
