import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';

@Pipe({ name: 'photoUrl' })
export class PhotoUrlPipe implements PipeTransform {
  transform(key: string, preview: boolean = false): string {
    if (!key) return '/assets/images/empty.jpg';

    return `${environment.mainApiUrl}Images/?key=${key}&imageSize=${
      preview ? 'Preview' : 'Full'
    }`;
  }
}
