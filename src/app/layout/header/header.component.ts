import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { authActions } from 'src/app/auth/data/auth-state/auth-actions';
import { authQuery } from 'src/app/auth/data/auth-state/auth-selectors';
import { CurrentUser } from 'src/app/core/models/current-user';

@Component({
  selector: 'sc-header',
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  currentUser$: Observable<CurrentUser | null>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.currentUser$ = this.store.select(authQuery.selectUser);
  }

  logout() {
    this.store.dispatch(authActions.logout());
  }
}
