import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PromoMeetingsWidgetModule } from '../meetings/promo-meetings-widget/promo-meetings-widget.module';
import { SharedModule } from '../shared/shared.module';
import { PromoUsersWidgetModule } from '../users/promo-users-widget/promo-users-widget.module';
import { HomeRoutingModule } from './home.routing.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    SharedModule,
    PromoMeetingsWidgetModule,
    PromoUsersWidgetModule,
    RouterModule,
    HomeRoutingModule,
  ],
})
export class HomeModule {}
