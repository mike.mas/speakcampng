import { createFeature, createReducer, on } from '@ngrx/store';
import { Review } from 'src/app/core/models/review';
import { userReviewsActions } from './user-reviews-actions';

export interface UserReviewsState {
  reviews: Review[];
  loading: boolean;
  loaded: boolean;
}

export const userReviewsInitialState: UserReviewsState = {
  reviews: [],
  loading: false,
  loaded: false,
};

export const userReviewsFeature = createFeature({
  name: 'userReviews',
  reducer: createReducer(
    userReviewsInitialState,

    on(userReviewsActions.loadUserReviews, (state) => {
      return { ...state, loading: true };
    }),
    on(userReviewsActions.loadUserReviewsSuccess, (state, { result }) => {
      return {
        ...state,
        reviews: result.items,
        loading: false,
        loaded: true,
      };
    }),
    on(userReviewsActions.loadUserReviewsFailure, (state) => {
      return {
        ...state,
        reviews: [],
        loading: false,
        loaded: false,
      };
    })
  ),
});
