import { userReviewsFeature } from './user-reviews-reducers';

const { selectReviews, selectLoading } = userReviewsFeature;

export const userReviewsQuery = {
  selectReviews,
  selectLoading,
};
