import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { ReviewsService } from '../services/reviews.service';
import { userReviewsActions } from './user-reviews-actions';

@Injectable()
export class UserReviewsEffects {
  loadUserReviews = createEffect(() =>
    this.actions$.pipe(
      ofType(userReviewsActions.loadUserReviews),
      switchMap((action) =>
        this.reviewsService.getUserReviews(action.id).pipe(
          map((result) =>
            userReviewsActions.loadUserReviewsSuccess({ result })
          ),
          catchError((error) =>
            of(userReviewsActions.loadUserReviewsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private reviewsService: ReviewsService,
    private store: Store
  ) {}
}
