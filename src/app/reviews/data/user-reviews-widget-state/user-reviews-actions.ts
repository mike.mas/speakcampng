import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/core/models/list-result';
import { Review } from 'src/app/core/models/review';

export const userReviewsActions = createActionGroup({
  source: 'User Reviews',
  events: {
    'Load User Reviews': props<{ id: number }>(),
    'Load User Reviews Failure': props<{ error: Error }>(),
    'Load User Reviews Success': props<{ result: ListResult<Review> }>(),
  },
});
