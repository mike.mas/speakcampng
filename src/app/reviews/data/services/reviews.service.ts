import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ListResult } from 'src/app/core/models/list-result';
import { Review } from 'src/app/core/models/review';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ReviewsService {
  constructor(private http: HttpClient) {}

  getUserReviews(id: number): Observable<ListResult<Review>> {
    return this.http.get(`${environment.mainApiUrl}Users/${id}/reviews`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  addReview(review: Review): Observable<number> {
    return this.http.post(`${environment.mainApiUrl}Reviews/`, review).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
}
