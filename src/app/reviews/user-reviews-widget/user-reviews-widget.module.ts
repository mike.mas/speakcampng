import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserReviewsWidget } from './user-reviews-widget.component';
import { userReviewsFeature } from '../data/user-reviews-widget-state/user-reviews-reducers';
import { UserReviewsEffects } from '../data/user-reviews-widget-state/user-reviews-effects';

@NgModule({
  declarations: [UserReviewsWidget],
  exports: [UserReviewsWidget],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(userReviewsFeature),
    EffectsModule.forFeature([UserReviewsEffects]),
  ],
})
export class UserReviewsWidgetModule {}
