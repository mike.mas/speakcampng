import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Review } from 'src/app/core/models/review';
import { userReviewsActions } from '../data/user-reviews-widget-state/user-reviews-actions';
import { userReviewsQuery } from '../data/user-reviews-widget-state/user-reviews-selectors';

@Component({
  selector: 'sc-user-reviews-widget',
  templateUrl: './user-reviews-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserReviewsWidget implements OnInit, OnChanges {
  @Input()
  userId: number;

  reviews$: Observable<Review[]>;

  constructor(private store: Store) {}

  ngOnChanges(): void {
    this.store.dispatch(
      userReviewsActions.loadUserReviews({ id: this.userId })
    );
  }

  ngOnInit(): void {
    this.reviews$ = this.store.select(userReviewsQuery.selectReviews);
  }
}
