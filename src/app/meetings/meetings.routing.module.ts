import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'meetings',
    loadChildren: () =>
      import('./meeting-list/meeting-list.module').then(
        (m) => m.MeetingListModule
      ),
  },
  {
    path: 'meeting/:id',
    loadChildren: () =>
      import('./meeting-details/meeting-details.module').then(
        (m) => m.MeetingDetailsModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  declarations: [],
})
export class MeetingsRoutingModule {}
