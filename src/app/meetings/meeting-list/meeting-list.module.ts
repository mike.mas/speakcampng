import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from 'src/app/shared/shared.module';
import { MeetingListComponent } from './meeting-list.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { meetingListFeature } from '../data/meeting-list-state/meeting-list-reducer';
import { EffectsModule } from '@ngrx/effects';
import { MeetingListEffects } from '../data/meeting-list-state/meeting-list-effects';
import { MeetingSortComponent } from './meeting-sort/meeting-sort.component';
import { MeetingFilterComponent } from './meeting-filter/meeting-filter.component';

@NgModule({
  declarations: [
    MeetingListComponent,
    MeetingSortComponent,
    MeetingFilterComponent,
  ],
  exports: [MeetingListComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(meetingListFeature),
    EffectsModule.forFeature([MeetingListEffects]),
    RouterModule.forChild([
      {
        path: '',
        component: MeetingListComponent,
      },
    ]),
  ],
})
export class MeetingListModule {}
