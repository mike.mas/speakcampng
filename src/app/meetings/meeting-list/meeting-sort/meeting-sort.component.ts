import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  DoCheck,
} from '@angular/core';

@Component({
  selector: 'sc-meeting-sort',
  templateUrl: './meeting-sort.component.html',
})
export class MeetingSortComponent {
  constructor() {}

  @Input()
  count: number | null;

  @Input()
  sort: string | null;

  @Output()
  sortChange: EventEmitter<string> = new EventEmitter<string>();

  onChange(event: Event) {
    this.sortChange.emit((event.target as HTMLSelectElement).value);
  }
}
