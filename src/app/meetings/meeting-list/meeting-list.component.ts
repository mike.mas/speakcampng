import {
  ChangeDetectionStrategy,
  Component,
  DoCheck,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { Meeting } from 'src/app/core/models/meeting';
import { meetingListActions } from '../data/meeting-list-state/meeting-list-actions';
import { meetingListQuery } from '../data/meeting-list-state/meeting-list-selectors';
import { MeetingFilter } from '../data/models/meeting-filter';

@Component({
  selector: 'sc-meeting-list',
  templateUrl: './meeting-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeetingListComponent implements OnInit {
  constructor(private store: Store) {}

  meetings$: Observable<Meeting[]> = this.store.select(
    meetingListQuery.selectItems
  );
  count$: Observable<number> = this.store.select(meetingListQuery.selectCount);
  sort$: Observable<string> = this.store.select(meetingListQuery.selectSorting);
  filter$: Observable<MeetingFilter> = this.store.select(
    meetingListQuery.selectFilter
  );

  ngOnInit(): void {
    this.store.dispatch(meetingListActions.loadMeetings());
  }

  onSortChange(value: string) {
    this.store.dispatch(
      meetingListActions.setMeetingsSorting({ sorting: value })
    );
  }

  onFilterChange(value: MeetingFilter) {
    this.store.dispatch(
      meetingListActions.setMeetingsFilter({ filter: value })
    );
  }
}
