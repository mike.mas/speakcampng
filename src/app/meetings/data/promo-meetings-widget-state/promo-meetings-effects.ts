import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { MeetingsService } from '../services/meetings.service';
import { promoMeetingsActions } from './promo-meetings-actions';

@Injectable()
export class PromoMeetingsEffects {
  loadUserMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(promoMeetingsActions.loadPromoMeetings),
      switchMap((action) =>
        this.meetingsService.getUpcomingMeetings(action.top).pipe(
          map((result) =>
            promoMeetingsActions.loadPromoMeetingsSuccess({ result })
          ),
          catchError((error) =>
            of(promoMeetingsActions.loadPromoMeetingsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private meetingsService: MeetingsService,
    private store: Store
  ) {}
}
