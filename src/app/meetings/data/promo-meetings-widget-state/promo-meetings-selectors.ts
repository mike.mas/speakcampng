import { promoMeetingsFeature } from './promo-meetings-reducers';

const { selectMeetings, selectLoading } = promoMeetingsFeature;

export const promoMeetingsQuery = {
  selectMeetings,
  selectLoading,
};
