import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/core/models/list-result';
import { Meeting } from 'src/app/core/models/meeting';

export const promoMeetingsActions = createActionGroup({
  source: 'Promo Meetings',
  events: {
    'Load Promo Meetings': props<{ top: number }>(),
    'Load Promo Meetings Failure': props<{ error: Error }>(),
    'Load Promo Meetings Success': props<{ result: ListResult<Meeting> }>(),
  },
});
