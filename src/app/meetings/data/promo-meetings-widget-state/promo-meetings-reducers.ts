import { createFeature, createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/core/models/meeting';
import { promoMeetingsActions } from './promo-meetings-actions';

export interface PromoMeetingsState {
  meetings: Meeting[];
  loading: boolean;
  loaded: boolean;
}

export const promoMeetingsInitialState: PromoMeetingsState = {
  meetings: [],
  loading: false,
  loaded: false,
};

export const promoMeetingsFeature = createFeature({
  name: 'promoMeetings',
  reducer: createReducer(
    promoMeetingsInitialState,

    on(promoMeetingsActions.loadPromoMeetings, (state) => {
      return { ...state, loading: true };
    }),
    on(promoMeetingsActions.loadPromoMeetingsSuccess, (state, { result }) => {
      return {
        ...state,
        meetings: result.items,
        loading: false,
        loaded: true,
      };
    }),
    on(promoMeetingsActions.loadPromoMeetingsFailure, (state) => {
      return {
        ...state,
        meetings: [],
        loading: false,
        loaded: false,
      };
    })
  ),
});
