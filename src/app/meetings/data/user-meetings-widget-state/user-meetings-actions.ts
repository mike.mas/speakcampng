import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/core/models/list-result';
import { Meeting } from 'src/app/core/models/meeting';

export const userMeetingsActions = createActionGroup({
  source: 'User Meetings',
  events: {
    'Load User Meetings': props<{ id: number }>(),
    'Load User Meetings Failure': props<{ error: Error }>(),
    'Load User Meetings Success': props<{ result: ListResult<Meeting> }>(),
  },
});
