import { createFeature, createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/core/models/meeting';
import { userMeetingsActions } from './user-meetings-actions';

export interface UserMeetingsState {
  meetings: Meeting[];
  loading: boolean;
  loaded: boolean;
}

export const userMeetingsInitialState: UserMeetingsState = {
  meetings: [],
  loading: false,
  loaded: false,
};

export const userMeetingsFeature = createFeature({
  name: 'userMeetings',
  reducer: createReducer(
    userMeetingsInitialState,

    on(userMeetingsActions.loadUserMeetings, (state) => {
      return { ...state, loading: true };
    }),
    on(userMeetingsActions.loadUserMeetingsSuccess, (state, { result }) => {
      return {
        ...state,
        meetings: result.items,
        loading: false,
        loaded: true,
      };
    }),
    on(userMeetingsActions.loadUserMeetingsFailure, (state) => {
      return {
        ...state,
        meetings: [],
        loading: false,
        loaded: false,
      };
    })
  ),
});
