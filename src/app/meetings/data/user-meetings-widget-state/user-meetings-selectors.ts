import { userMeetingsFeature } from './user-meetings-reducers';

const { selectMeetings, selectLoading } = userMeetingsFeature;

export const userMeetingsQuery = {
  selectMeetings,
  selectLoading,
};
