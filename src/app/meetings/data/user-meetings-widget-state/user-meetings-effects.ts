import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { MeetingsService } from '../services/meetings.service';
import { userMeetingsActions } from './user-meetings-actions';

@Injectable()
export class UserMeetingsEffects {
  loadUserMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(userMeetingsActions.loadUserMeetings),
      switchMap((action) =>
        this.meetingsService.getUserMeetings(action.id).pipe(
          map((result) =>
            userMeetingsActions.loadUserMeetingsSuccess({ result })
          ),
          catchError((error) =>
            of(userMeetingsActions.loadUserMeetingsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private meetingsService: MeetingsService,
    private store: Store
  ) {}
}
