import { meetingListFeature } from './meeting-list-reducer';

const { selectFilter, selectCount, selectItems, selectSorting, selectLoading } =
  meetingListFeature;

export const meetingListQuery = {
  selectFilter,
  selectCount,
  selectItems,
  selectSorting,
  selectLoading,
};
