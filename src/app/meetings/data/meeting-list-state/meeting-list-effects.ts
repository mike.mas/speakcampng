import { Injectable } from '@angular/core';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, switchMap, map, of } from 'rxjs';
import { MeetingsService } from '../services/meetings.service';
import { meetingListActions } from './meeting-list-actions';
import { meetingListQuery } from './meeting-list-selectors';

@Injectable()
export class MeetingListEffects {
  setMeetingsSorting = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingListActions.setMeetingsSorting),
      map(() => meetingListActions.loadMeetings())
    )
  );

  setMeetingsFilter = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingListActions.setMeetingsFilter),
      map(() => meetingListActions.loadMeetings())
    )
  );

  loadMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingListActions.loadMeetings),
      concatLatestFrom(() => [
        this.store.select(meetingListQuery.selectFilter),
        this.store.select(meetingListQuery.selectSorting),
      ]),
      switchMap(([action, filter, sorting]) =>
        this.meetingsService.getMeetingsList(sorting, filter).pipe(
          map((result) => meetingListActions.loadMeetingsSuccess({ result })),
          catchError((error) =>
            of(meetingListActions.loadMeetingsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private meetingsService: MeetingsService,
    private store: Store
  ) {}
}
