import { MeetingDetails } from 'src/app/core/models/meeting-details';

export interface MeetingActionEvent {
  meetingId: number;
  visitId?: number;
  action: 'join' | 'cancel';
}
