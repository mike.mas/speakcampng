import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DateHelper } from 'src/app/core/helpers/date-helper';
import { ListResult } from 'src/app/core/models/list-result';
import { Meeting } from 'src/app/core/models/meeting';
import { MeetingDetails } from 'src/app/core/models/meeting-details';
import { environment } from 'src/environments/environment';
import { MeetingFilter } from '../models/meeting-filter';

@Injectable({ providedIn: 'root' })
export class MeetingsService {
  constructor(private http: HttpClient, private dateHelper: DateHelper) {}

  getMeeting(id: number): Observable<MeetingDetails> {
    return this.http.get(`${environment.mainApiUrl}Meetings/${id}`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  getMeetingsList(
    sorting: string,
    filter: MeetingFilter
  ): Observable<ListResult<Meeting>> {
    return this.http
      .get(
        `${environment.mainApiUrl}Meetings?` +
          `Sorting=${sorting}` +
          `&LanguageKey=${filter.languageKey}` +
          `&LanguageLevel=${filter.languageLevel}` +
          `&DateFrom=${this.dateHelper.getUTCDateString(filter.dateFrom)}` +
          `&DateTo=${this.dateHelper.getUTCDateString(filter.dateTo)}`
      )
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getUpcomingMeetings(top: number = 3) {
    return this.http
      .get(
        `${environment.mainApiUrl}Meetings?Sorting=StartDate&PageSize=${top}`
      )
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getUserMeetings(id: number): Observable<ListResult<Meeting>> {
    return this.http.get(`${environment.mainApiUrl}Users/${id}/meetings`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  joinMeeting(id: number): Observable<number> {
    return this.http
      .post(`${environment.mainApiUrl}Visits`, { meetingId: id })
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  cancelJoinMeeting(visitId: number): Observable<boolean> {
    return this.http.delete(`${environment.mainApiUrl}Visits/${visitId}`).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
}
