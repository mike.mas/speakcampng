import { createSelector } from '@ngrx/store';
import { meetingDetailsFeature } from './meeting-details-reducer';

const { selectMeeting, selectLoading } = meetingDetailsFeature;
const selectVisits = createSelector(
  selectMeeting,
  (meeting) => meeting?.visits
);

export const meetingDetailsQuery = {
  selectMeeting,
  selectLoading,
  selectVisits,
};
