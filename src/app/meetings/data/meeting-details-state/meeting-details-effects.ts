import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, exhaustMap, map, of, switchMap } from 'rxjs';
import { MeetingsService } from '../services/meetings.service';
import { meetingDetailsActions } from './meeting-details-actions';

@Injectable()
export class MeetingDetailsEffects {
  loadDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingDetailsActions.loadMeetingDetails),
      switchMap((action) =>
        this.meetingsService.getMeeting(action.id).pipe(
          map((result) => {
            return meetingDetailsActions.loadMeetingDetailsSuccess({ result });
          }),
          catchError((error) =>
            of(meetingDetailsActions.loadMeetingDetailsFailure({ error }))
          )
        )
      )
    )
  );

  joinMeeting = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingDetailsActions.joinMeeting),
      exhaustMap((action) =>
        this.meetingsService.joinMeeting(action.id).pipe(
          map((result) =>
            meetingDetailsActions.joinMeetingSuccess({
              result,
              meetingId: action.id,
            })
          ),
          catchError((error) =>
            of(meetingDetailsActions.joinMeetingFailure({ error }))
          )
        )
      )
    )
  );

  cancelJoinMeeting = createEffect(() =>
    this.actions$.pipe(
      ofType(meetingDetailsActions.cancelJoinMeeting),
      exhaustMap((action) =>
        this.meetingsService.cancelJoinMeeting(action.visitId).pipe(
          map((result) =>
            meetingDetailsActions.cancelJoinMeetingSuccess({
              meetingId: action.id,
            })
          ),
          catchError((error) =>
            of(meetingDetailsActions.cancelJoinMeetingFailure({ error }))
          )
        )
      )
    )
  );

  afterMeetingActionSucceed = createEffect(() =>
    this.actions$.pipe(
      ofType(
        meetingDetailsActions.cancelJoinMeetingSuccess,
        meetingDetailsActions.joinMeetingSuccess
      ),
      map((action) =>
        meetingDetailsActions.loadMeetingDetails({ id: action.meetingId })
      )
    )
  );

  constructor(
    private actions$: Actions,
    private meetingsService: MeetingsService,
    private store: Store
  ) {}
}
