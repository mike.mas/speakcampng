import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { MeetingDetails } from 'src/app/core/models/meeting-details';

export const meetingDetailsActions = createActionGroup({
  source: 'Meeting Details',
  events: {
    'Load Meeting Details': props<{ id: number }>(),
    'Load Meeting Details Failure': props<{ error: Error }>(),
    'Load Meeting Details Success': props<{ result: MeetingDetails }>(),

    'Join Meeting': props<{ id: number }>(),
    'Join Meeting Success': props<{ result: number; meetingId: number }>(),
    'Join Meeting Failure': props<{ error: Error }>(),

    'Cancel Join Meeting': props<{ id: number; visitId: number }>(),
    'Cancel Join Meeting Success': props<{ meetingId: number }>(),
    'Cancel Join Meeting Failure': props<{ error: Error }>(),
  },
});
