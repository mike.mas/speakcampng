import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

import { MeetingDetailsComponent } from './meeting-details.component';
import { MeetingSummaryComponent } from './meeting-summary/meeting-summary.component';
import { MeetingVisitsComponent } from './meeting-visits/meeting-visits.component';
import { MeetingSummaryActionsComponent } from './meeting-summary/meeting-summary-actions/meeting-summary-actions.component';
import { meetingDetailsFeature } from '../data/meeting-details-state/meeting-details-reducer';
import { MeetingDetailsEffects } from '../data/meeting-details-state/meeting-details-effects';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  declarations: [
    MeetingDetailsComponent,
    MeetingSummaryComponent,
    MeetingVisitsComponent,
    MeetingSummaryActionsComponent,
  ],
  exports: [MeetingDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(meetingDetailsFeature),
    EffectsModule.forFeature([MeetingDetailsEffects]),

    RouterModule.forChild([
      {
        path: '',
        component: MeetingDetailsComponent,
      },
    ]),
  ],
})
export class MeetingDetailsModule {}
