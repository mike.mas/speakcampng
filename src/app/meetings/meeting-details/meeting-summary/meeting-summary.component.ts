import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CurrentUser } from 'src/app/core/models/current-user';
import { MeetingDetails } from 'src/app/core/models/meeting-details';
import { MeetingActionEvent } from '../../data/models/meeting-action-event';

@Component({
  selector: 'sc-meeting-summary',
  templateUrl: './meeting-summary.component.html',
})
export class MeetingSummaryComponent implements OnInit {
  @Input()
  authUser: CurrentUser | null;

  @Input()
  meeting: MeetingDetails;

  @Output()
  meetingAction: EventEmitter<MeetingActionEvent> = new EventEmitter<MeetingActionEvent>();

  constructor() {}

  ngOnInit(): void {}

  onMeetingAction(event: MeetingActionEvent) {
    this.meetingAction.emit(event);
  }
}
