import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { MeetingDetails } from 'src/app/core/models/meeting-details';
import { Store } from '@ngrx/store';
import { meetingDetailsActions } from '../data/meeting-details-state/meeting-details-actions';
import { meetingDetailsQuery } from '../data/meeting-details-state/meeting-details-selectors';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MeetingActionEvent } from '../data/models/meeting-action-event';
import { CurrentUser } from 'src/app/core/models/current-user';
import { authQuery } from 'src/app/auth/data/auth-state/auth-selectors';

@Component({
  selector: 'sc-meeting-details',
  templateUrl: './meeting-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@UntilDestroy()
export class MeetingDetailsComponent implements OnInit {
  meeting$: Observable<MeetingDetails | null>;
  authUser$: Observable<CurrentUser | null>;

  constructor(private route: ActivatedRoute, private store: Store) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];

    this.store.dispatch(meetingDetailsActions.loadMeetingDetails({ id }));

    this.meeting$ = this.store.select(meetingDetailsQuery.selectMeeting);
    this.authUser$ = this.store.select(authQuery.selectUser);
  }

  onMeetingAction(event: MeetingActionEvent) {
    if (event.action == 'join') {
      this.store.dispatch(
        meetingDetailsActions.joinMeeting({ id: event.meetingId })
      );
    } else if (event.action == 'cancel' && event.visitId) {
      this.store.dispatch(
        meetingDetailsActions.cancelJoinMeeting({
          id: event.meetingId,
          visitId: event.visitId,
        })
      );
    }
  }
}
