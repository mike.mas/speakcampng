import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Meeting } from 'src/app/core/models/meeting';
import { userMeetingsActions } from '../data/user-meetings-widget-state/user-meetings-actions';
import { userMeetingsQuery } from '../data/user-meetings-widget-state/user-meetings-selectors';

@Component({
  selector: 'sc-user-meetings-widget',
  templateUrl: './user-meetings-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMeetingsWidget implements OnInit, OnChanges {
  @Input()
  userId: number;

  meetings$: Observable<Meeting[]>;

  constructor(private store: Store) {}

  ngOnChanges(): void {
    this.store.dispatch(
      userMeetingsActions.loadUserMeetings({ id: this.userId })
    );
  }

  ngOnInit(): void {
    this.meetings$ = this.store.select(userMeetingsQuery.selectMeetings);
  }
}
