import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserMeetingsWidget } from './user-meetings-widget.component';
import { UserMeetingsEffects } from '../data/user-meetings-widget-state/user-meetings-effects';
import { userMeetingsFeature } from '../data/user-meetings-widget-state/user-meetings-reducers';

@NgModule({
  declarations: [UserMeetingsWidget],
  exports: [UserMeetingsWidget],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(userMeetingsFeature),
    EffectsModule.forFeature([UserMeetingsEffects]),
  ],
})
export class UserMeetingsWidgetModule {}
