import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PromoMeetingsWidget } from './promo-meetings-widget.component';
import { PromoMeetingsEffects } from '../data/promo-meetings-widget-state/promo-meetings-effects';
import { promoMeetingsFeature } from '../data/promo-meetings-widget-state/promo-meetings-reducers';

@NgModule({
  declarations: [PromoMeetingsWidget],
  exports: [PromoMeetingsWidget],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(promoMeetingsFeature),
    EffectsModule.forFeature([PromoMeetingsEffects]),
  ],
})
export class PromoMeetingsWidgetModule {}
