import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Meeting } from 'src/app/core/models/meeting';
import { promoMeetingsActions } from '../data/promo-meetings-widget-state/promo-meetings-actions';
import { promoMeetingsQuery } from '../data/promo-meetings-widget-state/promo-meetings-selectors';

@Component({
  selector: 'sc-promo-meetings-widget',
  templateUrl: './promo-meetings-widget.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PromoMeetingsWidget implements OnInit {
  @Input()
  top: number = 3;

  meetings$: Observable<Meeting[]>;

  constructor(private store: Store) {}

  ngOnChanges(): void {
    this.store.dispatch(
      promoMeetingsActions.loadPromoMeetings({ top: this.top })
    );
  }

  ngOnInit(): void {
    this.meetings$ = this.store.select(promoMeetingsQuery.selectMeetings);
  }
}
