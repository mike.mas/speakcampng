import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { CurrentUser } from 'src/app/core/models/current-user';
import { AuthResponse } from '../models/auth-response';
import { LoginRequest } from '../models/login-request';
import { RegisterRequest } from '../models/register-request';

export const authActions = createActionGroup({
  source: 'Auth',
  events: {
    Login: props<{ request: LoginRequest }>(),
    'Login Success': props<{ result: AuthResponse }>(),
    'Login Failure': props<{ message: string }>(),

    Register: props<{ request: RegisterRequest }>(),
    'Register Success': props<{ result: AuthResponse }>(),
    'Register Failure': props<{ message: string }>(),

    'Load CurrentUser': props<{ accountId: number }>(),
    'Load CurrentUser Success': props<{ result: CurrentUser }>(),
    'Load CurrentUser Failure': props<{ message: string }>(),

    Logout: emptyProps(),
    'Logout Success': emptyProps(),
    'Logout Failure': emptyProps(),

    LoadToken: emptyProps(),
    'LoadToken Success': props<{ result: AuthResponse }>(),
    'LoadToken Failure': emptyProps(),
  },
});
