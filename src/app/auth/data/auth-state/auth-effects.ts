import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { StorageHelper } from 'src/app/core/helpers/storage-helper';
import { AuthFacade } from '../services/auth.facade';
import { authActions } from './auth-actions';
import { authQuery } from './auth-selectors';

const STORAGE_TOKEN_KEY: string = 'AuthToken';

@Injectable()
export class AuthEffects {
  login = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.login),
      switchMap((action) =>
        this.authFacade.login(action.request).pipe(
          map((result) =>
            result.successful
              ? authActions.loginSuccess({ result })
              : authActions.loginFailure({ message: 'Wrong email or password' })
          ),
          catchError((error: HttpErrorResponse) =>
            of(authActions.loginFailure({ message: error.message }))
          )
        )
      )
    )
  );

  register = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.register),
      switchMap((action) =>
        this.authFacade.register(action.request).pipe(
          map((result) =>
            result.successful
              ? authActions.registerSuccess({ result })
              : authActions.registerFailure({ message: 'User already exists' })
          ),
          catchError((error: HttpErrorResponse) =>
            of(authActions.registerFailure({ message: error.message }))
          )
        )
      )
    )
  );

  loadCurrentUser = createEffect(() =>
    this.actions$.pipe(
      ofType(
        authActions.loginSuccess,
        authActions.registerSuccess,
        authActions.loadtokenSuccess
      ),
      concatLatestFrom(() => [this.store.select(authQuery.selectToken)]),
      switchMap(([action, token]) =>
        this.authFacade.getCurrentUser(token?.accountId ?? 0).pipe(
          map((result) => authActions.loadCurrentuserSuccess({ result })),
          catchError((error: HttpErrorResponse) =>
            of(authActions.loadCurrentuserFailure({ message: error.message }))
          )
        )
      )
    )
  );

  logout = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.logout),
      map((_) => authActions.logoutSuccess())
    )
  );

  saveSessionToken = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.loginSuccess, authActions.registerSuccess),
        tap((action) => {
          this.storageHelper.set(STORAGE_TOKEN_KEY, action.result);
        })
      ),
    { dispatch: false }
  );

  clearSessionToken = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.logoutSuccess),
        tap((action) => {
          this.storageHelper.set(STORAGE_TOKEN_KEY, null);
        })
      ),
    { dispatch: false }
  );

  loadSessionToken = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.loadtoken),
      map((action) => {
        const token = this.storageHelper.get(STORAGE_TOKEN_KEY);
        return token
          ? authActions.loadtokenSuccess({ result: token })
          : authActions.loadtokenFailure();
      })
    )
  );

  redirectAfterLogin$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.loginSuccess),
        tap(() => {
          this.router.navigate(['/']);
        })
      ),
    { dispatch: false }
  );

  redirectAfterRegistration$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.registerSuccess),
        tap(() => {
          this.router.navigate(['/']);
        })
      ),
    { dispatch: false }
  );

  redirectAfterLogout$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.logoutSuccess),
        tap(() => {
          this.router.navigate(['/']);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private authFacade: AuthFacade,
    private store: Store,
    private router: Router,
    private storageHelper: StorageHelper
  ) {}
}
