export interface CreateUserRequest {
  userType: string;
  firstName: string;
  lastName: string;
  languageKey: string;
  languageLevel: string;
  timezoneOffsetMinutes: number;
}
