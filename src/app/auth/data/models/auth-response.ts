export interface AuthResponse {
  accessToken?: string;
  refreshToken?: string;
  successful: boolean;
}
