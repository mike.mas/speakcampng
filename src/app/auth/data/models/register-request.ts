export interface RegisterRequest {
  role: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  languageKey: string;
  languageLevel: string;
}
