import { AuthToken } from '../models/auth-token';
import jwt_decode from 'jwt-decode';

export class TokenHelper {
  static decode(
    accessToken: string | undefined,
    refreshToken: string | undefined
  ): AuthToken | null {
    if (!accessToken || !refreshToken) return null;
    const decodedToken = jwt_decode<any>(accessToken);
    return <AuthToken>{
      accountId:
        decodedToken[
          'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'
        ],
      expDate: decodedToken['exp'],
      accessToken: accessToken,
      refreshToken: refreshToken,
    };
  }
}
