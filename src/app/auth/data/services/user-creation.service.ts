import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CurrentUser } from 'src/app/core/models/current-user';
import { environment } from 'src/environments/environment';
import { CreateUserRequest } from '../models/create-user-request';

@Injectable({ providedIn: 'root' })
export class UserCreationService {
  constructor(private http: HttpClient) {}

  create(request: CreateUserRequest, authToken?: string): Observable<number> {
    return this.http
      .post(
        `${environment.mainApiUrl}Users`,
        request,
        this.getAuthOptions(authToken)
      )
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  getByAccountId(accountId: number): Observable<CurrentUser> {
    return this.http
      .get(`${environment.mainApiUrl}Users/byaccount/${accountId}`)
      .pipe(
        map((response: any) => {
          return response;
        })
      );
  }

  private getAuthOptions(authToken: string | undefined) {
    if (authToken)
      return {
        headers: new HttpHeaders().append(
          'Authorization',
          `Bearer ${authToken}`
        ),
      };

    return undefined;
  }
}
