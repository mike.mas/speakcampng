import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { authActions } from '../data/auth-state/auth-actions';
import { authQuery } from '../data/auth-state/auth-selectors';

@Component({
  selector: 'sc-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  constructor(private fb: FormBuilder, private store: Store) {}
  form: FormGroup;
  error$: Observable<string | null>;
  submitted: boolean = false;

  ngOnInit(): void {
    this.initForm();

    this.error$ = this.store.select(authQuery.selectError);
  }

  initForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
    });
  }

  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }

  submit(form: FormGroup) {
    if (!form.valid) return;

    this.store.dispatch(authActions.login({ request: form.value }));
    this.submitted = true;
  }
}
